
#!/bin/bash -ex

##################################################################################################################################################
#This script expects 6 arguments to run correctly                                                                                                #
#syntax to run script ./createdb.sh "Root DB Name" "Path/To/SSM/Username" "Path/To/SSM/Password" "New DB Name" "New DB User" "New Users Password"#
##################################################################################################################################################

if [ "$#" -ne 6 ]; then
  echo "Incorrect number of arguments passed"
  exit 1
fi

export Host=$(aws rds describe-db-instances  --db-instance-identifier $1 --output text --query DBInstances[*].Endpoint.Address)
export DBname=$(aws rds describe-db-instances  --db-instance-identifier $1 --output text --query DBInstances[*].DBName)
export User=$(aws ssm get-parameter --name "$2" --output text --query Parameter.Value)
export Password=$(aws ssm get-parameter --name "$3" --with-decryption --output text --query Parameter.Value)

mysql -u $User -p$Password -h $Host -D $DBname <<EOF
CREATE DATABASE $4;
CREATE USER '$5'@'%' IDENTIFIED BY '$6';
GRANT ALL ON $4.* TO '$5'@'%';
EOF

aws ssm put-parameter --name "/wordpress/$4/DBName" --type "String" --value $4 --overwrite

aws ssm put-parameter --name "/wordpress/$4/UserName" --type "String" --value $5 --overwrite

aws ssm put-parameter --name "/wordpress/$4/Password" --type "SecureString" --value $6 --overwrite
