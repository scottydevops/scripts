#!/bin/bash +ex

curl -X GET \
  https://api.cloudradar.io/v1/hosts/ \
  -H 'Authorization: Bearer c8ad007d3a248cad70cc94c3fea32c2a9e30bc1fa0aa9876' >$

StaleHosts=$(jq '.hosts[] | select(.active==false) | .uuid' hosts.json)

echo $StaleHosts
