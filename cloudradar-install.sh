#!/bin/bash -ex

export ID=$(curl http://169.254.169.254/latest/meta-data/instance-id)
export IP=$(curl http://169.254.169.254/latest/meta-data/local-ipv4)
export API_Key=$(aws ssm get-parameter --name /Platform/Monitoring/API_Key --with-decryption --output text --query Parameter.Value)

curl -X POST \
  https://api.cloudradar.io/v1/hosts/ \
  -H "Authorization: Bearer $API_Key" \
  -d '{
  "name":"Wordpress-Cluster-'$ID'",
  "connect":"'$IP'",
  "description":"Wordpress ASG",
  "tags": ["Dev"],
  "cagent":true,
  "dashboard":true
}' > /responce.json

HUB_USER=$(jq '.host .uuid' -r /responce.json) && echo $HUB_USER
HUB_URL=$(jq '.host .hub_url' -r /responce.json) && echo $HUB_URL
HUB_PASSWORD=$(jq '.host .hub_password' -r /responce.json) && echo $HUB_PASSWORD

curl -O https://repo.cloudradar.io/pool/utils/c/cloudradar-release/cloudradar-release.deb
sudo dpkg -i cloudradar-release.deb
sudo apt-get update
sudo \
CAGENT_HUB_URL=$HUB_URL \
CAGENT_HUB_USER=$HUB_USER \
CAGENT_HUB_PASSWORD=$HUB_PASSWORD \
apt-get install cagent
