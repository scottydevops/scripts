#!/bin/bash -ex
rm -rf /files
git clone https://Andyscott1547@bitbucket.org/scottydevops/files.git /files

if [ "$1" == "pegasus-technology-com" ]; then
  export DB_User=ptadmin
  export DB=ptdb
elif [ "$1" == "pegasus-technology-co-uk" ]; then
  export DB_User=ptcomadmin
  export DB=ptcomdb
elif [ "$1" == "homefarmproduce" ]; then
  export DB_User=hfpadmin
  export DB=hfpdb
elif [ "$1" == "techologists" ]; then
  export DB_User=tpadmin
  export DB=tdb
else
  echo "Incorrect Site Name Provided"
  exit 1
fi

if [ "$2" == "Dev" ]; then
  export db_id=$(jq '.Dev .DB_Name' -r /files/wordpress.config)
  export user_path=$(jq '.Dev .User_Path' -r /files/wordpress.config)
  export password_path=$(jq '.Dev .Password_Path' -r /files/wordpress.config)
elif [ "$2" == "QA" ]; then
  export db_id=$(jq '.QA .DB_Name' -r /files/wordpress.config)
  export user_path=$(jq '.QA .User_Path' -r /files/wordpress.config)
  export password_path=$(jq '.QA .Password_Path' -r /files/wordpress.config)
elif [ "$2" == "Prod" ]; then
  export db_id=$(jq '.Prod .DB_Name' -r /files/wordpress.config)
  export user_path=$(jq '.Prod .User_Path' -r /files/wordpress.config)
  export password_path=$(jq '.Prod .Password_Path' -r /files/wordpress.config)
else
  echo "Incorrect Env set or missing configuration"
  exit 1
fi

curl -O https://wordpress.org/latest.tar.gz
tar xzvf latest.tar.gz
touch ./wordpress/.htaccess
mkdir -p ./wordpress/wp-content/upgrade

if [ "$1" == "pegasus-technology-com" ]; then
  cp -a ./wordpress/. /efs/pegasus-technology-com
  cp ./files/wp-config-pegasus-technology-com.php /efs/pegasus-technology-com/wp-config.php
  chown -R www-data:www-data /efs/pegasus-technology-com
  find /efs/pegasus-technology-com -type d -exec chmod 750 {} \;
  find /efs/pegasus-technology-com -type f -exec chmod 640 {} \;
elif [ "$1" == "pegasus-technology-co-uk" ]; then
  cp -a ./wordpress/. /efs/pegasus-technology-co-uk
  cp ./files/wp-config-pegasus-technology-co-uk.php /efs/pegasus-technology-co-uk/wp-config.php
  chown -R www-data:www-data /efs/pegasus-technology-co-uk
  find /efs/pegasus-technology-co-uk -type d -exec chmod 750 {} \;
  find /efs/pegasus-technology-co-uk -type f -exec chmod 640 {} \;
elif [ "$1" == "homefarmproduce" ]; then
  cp -a ./wordpress/. /efs/homefarmproduce
  cp ./files/wp-config-homefarmproduce.php /efs/homefarmproduce/wp-config.php
  chown -R www-data:www-data /efs/homefarmproduce
  find /efs/homefarmproduce -type d -exec chmod 750 {} \;
  find /efs/homefarmproduce -type f -exec chmod 640 {} \;
elif [ "$1" == "techologists" ]; then
  cp -a ./wordpress/. /efs/techologists
  cp ./files/wp-config-techologists.php /efs/techologists/wp-config.php
  chown -R www-data:www-data /efs/techologists
  find /efs/techologists -type d -exec chmod 750 {} \;
  find /efs/techologists -type f -exec chmod 640 {} \;
else
  echo "Incorrect Site Name Provided"
  rm -rf /wordpress
  rm -rf latest.tar.gz
  exit 1
fi

export Host=$(aws rds describe-db-instances  --db-instance-identifier "$db_id" --output text --query DBInstances[*].Endpoint.Address)
export DBname=$(aws rds describe-db-instances  --db-instance-identifier "$db_id" --output text --query DBInstances[*].DBName)
export User=$(aws ssm get-parameter --name "$user_path" --output text --query Parameter.Value)
export Password=$(aws ssm get-parameter --name "$password_path" --with-decryption --output text --query Parameter.Value)
export New_Password=$(</dev/urandom tr -dc '12345qwertQWERTasdfgASDFGzxcvbZXCVB' | head -c8; echo "")

mysql -u $User -p$Password -h $Host -D $DBname <<EOF
CREATE DATABASE $DB;
CREATE USER '$DB_User'@'%' IDENTIFIED BY '$New_Password';
GRANT ALL ON $DB.* TO '$DB_User'@'%';
EOF

aws ssm put-parameter --name "/$1/$2/DBName" --type "String" --value $DB --overwrite
aws ssm put-parameter --name "/$1/$2/UserName" --type "String" --value $DB_User --overwrite
aws ssm put-parameter --name "/$1/$2/Password" --type "SecureString" --value $New_Password --overwrite

sed -i "/DB_HOST/s/'[^']*'/'$Host'/2" /efs/$1/wp-config.php
sed -i "/DB_NAME/s/'[^']*'/'$DB'/2" /efs/$1/wp-config.php
sed -i "/DB_USER/s/'[^']*'/'$DB_User'/2" /efs/$1/wp-config.php
sed -i "/DB_PASSWORD/s/'[^']*'/'$New_Password'/2" /efs/$1/wp-config.php

rm -rf /wordpress
rm -rf /latest.tar.gz
rm -rf /files
