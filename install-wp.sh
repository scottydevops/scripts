#!/bin/bash -ex

curl -O https://wordpress.org/latest.tar.gz
tar xzvf latest.tar.gz
touch ./wordpress/.htaccess
mkdir -p ./wordpress/wp-content/upgrade

if [ "$1" == "pegasus-technology-com" ]; then
  cp -a ./wordpress/. /efs/pegasus-technology-com
  git clone https://Andyscott1547@bitbucket.org/scottydevops/files.git
  cp ./files/wp-config-pegasus-technology-com.php /efs/pegasus-technology-com/wp-config.php
  chown -R www-data:www-data /efs/pegasus-technology-com
  find /efs/pegasus-technology-com -type d -exec chmod 750 {} \;
  find /efs/pegasus-technology-com -type f -exec chmod 640 {} \;
elif [ "$1" == "pegasus-technology-co-uk" ]; then
  cp -a ./wordpress/. /efs/pegasus-technology-co-uk
  git clone https://Andyscott1547@bitbucket.org/scottydevops/files.git
  cp ./files/wp-config-pegasus-technology-co-uk.php /efs/pegasus-technology-co-uk/wp-config.php
  chown -R www-data:www-data /efs/pegasus-technology-co-uk
  find /efs/pegasus-technology-co-uk -type d -exec chmod 750 {} \;
  find /efs/pegasus-technology-co-uk -type f -exec chmod 640 {} \;
elif [ "$1" == "homefarmproduce" ]; then
  cp -a ./wordpress/. /efs/homefarmproduce
  git clone https://Andyscott1547@bitbucket.org/scottydevops/files.git
  cp ./files/wp-config-homefarmproduce.php /efs/homefarmproduce/wp-config.php
  chown -R www-data:www-data /efs/homefarmproduce
  find /efs/homefarmproduce -type d -exec chmod 750 {} \;
  find /efs/homefarmproduce -type f -exec chmod 640 {} \;
else
  echo "Incorrect Site Name Provided"
  exit 1
fi

rm -rf /wordpress
rm -rf /latest.tar.gz
rm -rf /files
