#!/bin/bash +ex

aws ssm get-parameter --name "/Dev/Toca/ssh" --with-decryption --output text --query Parameter.Value >> ~/.ssh/id_rsa.pub
aws ssm get-parameter --name "/Dev/Toca/ssh-key" --with-decryption --output text --query Parameter.Value >> ~/.ssh/id_rsa
chmod 600 ~/.ssh/id_rsa
chmod 600 ~/.ssh/id_rsa.pub

git config --global core.sshCommand 'ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no'

cd /
git clone https://Andyscott1547@bitbucket.org/scottydevops/files.git
git clone git@gitlab.com:rogueco/toca-backend.git
git clone git@gitlab.com:rogueco/tocal-portal.git

cp /files/api.service /etc/systemd/system/api.service
cp /files/toco-reverse-proxy.conf /etc/apache2/conf-enabled/api.conf

cd /toca-backend
dotnet build
dotnet publish --output "/var/www/api"

a2enmod proxy proxy_http proxy_html headers rewrite
service apache2 restart
#echo Listen 8080 >> /etc/apache2/ports.conf

systemctl daemon-reload
sudo systemctl enable api.service
sudo systemctl start api.service

a2enmod rewrite
a2enmod headers

cd /Tocal-portal

curl -sL https://deb.nodesource.com/setup_10.x -o nodesource_setup.sh
sudo bash nodesource_setup.sh
sudo apt-get install nodejs -y
sudo npm install -g create-react-app
sudo npm install --unsafe-perm -g node-sass
npm install
npm run build --unsafe-perm
sudo npm install -g serve --unsafe-perm
serve -s build

#https://blog.todotnet.com/2017/07/publishing-and-running-your-asp-net-core-project-on-linux/
#https://phoenixnap.com/kb/update-node-js-version
#tocards.cbotqxwgl5e5.eu-west-1.rds.amazonaws.com
#TocaDB
#tocaadmin
#Supersecretpassword

#.AllowAnyOrigin()

#npm start --port 8000

# Always set these headers.
Header always set Access-Control-Allow-Origin "*"
Header always set Access-Control-Allow-Methods "POST, GET, OPTIONS, DELETE, PUT"
Header always set Access-Control-Max-Age "1000"
Header always set Access-Control-Allow-Headers "x-requested-with, Content-Type, origin, authorization, accept, client-security-token"

# Added a rewrite to respond with a 200 SUCCESS on every OPTIONS request.
RewriteEngine On
RewriteCond %{REQUEST_METHOD} OPTIONS
RewriteRule ^(.*)$ $1 [R=200,L]
