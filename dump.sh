#!/bin/bash

export Host=$(aws rds describe-db-instances  --db-instance-identifier wordpressrds --output text --query DBInstances[*].Endpoint.Address)
export DBname=$(aws rds describe-db-instances  --db-instance-identifier wordpressrds --output text --query DBInstances[*].DBName)
export User=$(aws ssm get-parameter --name "/Dev/wordpress/db_user" --output text --query Parameter.Value)
export Password=$(aws ssm get-parameter --name "/Dev/wordpress/db_password" --with-decryption --output text --query Parameter.Value)

sed -i "/DB_HOST/s/'[^']*'/'$Host'/2" wp-config.php
sed -i "/DB_NAME/s/'[^']*'/'$DBname'/2" wp-config.php
sed -i "/DB_USER/s/'[^']*'/'$User'/2" wp-config.php
sed -i "/DB_PASSWORD/s/'[^']*'/'$Password'/2" wp-config.php

define('WP_HOME','https://blog.lawrencemcdaniel.com');
define('WP_SITEURL','https://blog.lawrencemcdaniel.com');

// Update 8-April-2018: I moved https redirection from the Apache virtual server config to wp-config.php using this snippet.
if (strpos($_SERVER['HTTP_X_FORWARDED_PROTO'], 'https') !== false)
       $_SERVER['HTTPS']='on';


mysql -u $User -p$Password -h $Host -D $DBname <<EOF
CREATE DATABASE PTcom;
CREATE USER 'PT-COM'@'%' IDENTIFIED BY 'Onthe8thday';
GRANT ALL ON PTcom.* TO 'PT-COM'@'%';


mysql -u $User -p$Password -h $Host -D $DBname <<EOF
CREATE DATABASE $1;
CREATE USER '$2'@'%' IDENTIFIED BY '$3';
GRANT ALL ON $1.* TO '$2'@'%';

aws ssm put-parameter --name "authtoken" --type "String" --value "foobar" | jq -r .Parameter.Value

1  db name - wordpressrds
2 user name path - /Dev/wordpress/db_user
3 password path - /Dev/wordpress/db_password
4 new db name
5 new db user
6 new passowrd
