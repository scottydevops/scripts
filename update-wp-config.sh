#!/bin/bash

##################################################################################################
#This script expects 6 arguments to run correctly                                                #
#syntax to run script ./createdb.sh "Root DB Name" "Path/To/SSM/Username" "Path/To/SSM/Password" #
##################################################################################################

if [ "$#" -ne 3 ]; then
  echo "Incorrect number of arguments passed"
  exit 1
fi

export Host=$(aws rds describe-db-instances  --db-instance-identifier $1 --output text --query DBInstances[*].Endpoint.Address)
export DBname=$(aws rds describe-db-instances  --db-instance-identifier $1 --output text --query DBInstances[*].DBName)
export User=$(aws ssm get-parameter --name "$2" --output text --query Parameter.Value)
export Password=$(aws ssm get-parameter --name "$3" --with-decryption --output text --query Parameter.Value)

sed -i "/DB_HOST/s/'[^']*'/'$Host'/2" wp-config.php
sed -i "/DB_NAME/s/'[^']*'/'$DBname'/2" wp-config.php
sed -i "/DB_USER/s/'[^']*'/'$User'/2" wp-config.php
sed -i "/DB_PASSWORD/s/'[^']*'/'$Password'/2" wp-config.php
